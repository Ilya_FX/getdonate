package ru.ilyafx.gd;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GetDonate extends JavaPlugin implements Listener {

    private static String LINK;

    public void onEnable(){
        saveDefaultConfig();
        Bukkit.getPluginManager().registerEvents(this,this);
        LINK = getConfig().getString("link");

        Bukkit.getScheduler().runTaskTimerAsynchronously(this, ()->{
            Bukkit.getOnlinePlayers().forEach(player ->{
                String str = executeGet(player.getName());
                if(!str.isEmpty()){
                    if(!str.equalsIgnoreCase("")){
                        player.sendMessage(ChatColor.GREEN + "Вы получили донат!");
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(),str);
                    }
                }
            });
        },300,300);
    }

    @EventHandler
    public void join(PlayerJoinEvent event){
        Bukkit.getScheduler().runTaskAsynchronously(this, ()-> {
                String str = executeGet(event.getPlayer().getName());
                if(str.isEmpty()) return;
                switch(str){
                    case "":
                        break;
                    default:
                        event.getPlayer().sendMessage(ChatColor.GREEN + "Вы получили донат!");
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(),str);
                        break;
                }
        });
    }


    public String executeGet(String name) {
        try {
            String url = LINK.replace("%player%",name);

            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);

            request.addHeader("User-Agent", "Mozilla/5.0");

            HttpResponse response = client.execute(request);

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        } catch (Exception e) {
            return "none";
        }
    }


}
